# JF tests 
### Installation
you should setup your $GOPATH properely  

### Usage
```
Usage: jetflore-api-parser [-flags] -i [file] -o [file]
Flags:
  -i string
    	Parse from a file given a name
  -o string
    	Output file path
  -t string
    	Type to parse
	avaliable type:xml,json (default "xml")
```

## Knows issues
- [ ] No Windows Support
- [ ] JSON/XML no nesting style.
- [ ] No tests sorry.