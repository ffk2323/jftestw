package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"./libs"
	"github.com/clbanning/mxj"
	"errors"
        "github.com/mitchellh/mapstructure"
	"strconv"
	"time"
)

func usage() {
	fmt.Println("Usage: " + os.Args[0] + " [-flags] [file]")
	fmt.Println("Flags:")
	flag.PrintDefaults()
}
type Payloads struct {
	User_id string
	Limit string
	Subject_format string
}

type Payload struct {
	UserID string
	Limit int
}

type Output struct{
	USER_ID string
	SUBJECT []string
	DATE time.Time
}
var data []string =[]string{"item1", "item2", "item3"}

func main() {
	// Flags type, path to a file,
	ParseType := flag.String("t", "xml", "Input file type  format:xml,json")
	FileInput := flag.String("i", "", "Path to a file.")
	FileOutput := flag.String("o", "output.csv", "Path to a file. Default output.csv in workdir")
	// The custom lib feature, keep for support
	flag.Parse()
	if (*FileInput == "") || (flag.NArg() == 0) || ((*ParseType != "xml") && (*ParseType != "json" )) {

		usage()
	}
	bytes, err := ioutil.ReadFile(*FileInput)
	if err != nil {
		panic(err)
	}

	mapXj := XmlJson2Struct(bytes, *ParseType)
	values, err := mapXj.ValuesForPath("user_list.content")
	if err != nil {
		fmt.Println("err:", err.Error())
	}
	var result Payloads
	for _, v := range values {

		fmt.Println("v:", v)
		tmap := v.(map[string]interface{})
		c, _ := tmap["Subject_format"]
		//limit,_ := tmap["limit"]
		fmt.Println("c:", c)
		//fmt.Println("limit:", limit)
		if err := mapstructure.Decode(v, &result); err != nil {
			panic(err)
		}
		fmt.Printf("%+v", result)
		limitint,_ :=  strconv.Atoi(result.Limit);
		a := Payload{UserID: result.User_id, Limit: limitint}
		testdata := Output{USER_ID: a.UserID, SUBJECT: v, DATE: time.Now()}
		fmt.Printf("%+v", a)
		rb := jfapi.NewRequest()
		resp,err := rb.GetDo(*a)
		if err != nil && resp == nil{
			testdata = Output{USER_ID: a.UserID, SUBJECT: c, DATE: time.Now()}
		}

		data, err = jfapi.ParseResponse(resp)
		if err != nil {
			panic(err)
		}

		writero, err := jfapi.CsvWriter(*testdata,FileOutput)
		if err !=nil {
			panic(err)
		}
		fmt.Printf("%+v", writero)


	}

	if err != nil {
		fmt.Println("err:", err.Error())
		return
	}


}
func XmlJson2Struct(data []byte, typeName string) (mxj.Map) {
	if typeName == "xml" {
		m, err := mxj.NewMapXml(data)
		if err != nil {
			panic(err)
		}
		return m
	} else if typeName == "json" {
		m, err := mxj.NewMapJson(data)
		if err != nil {
			panic(err)
		}
		return m
	} else {
		panic(errors.New("Unexpected type"))
	}
}

