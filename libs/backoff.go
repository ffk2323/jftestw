package jfapi
//
// performs automatic retries under certain conditions.  if
// an error is returned by the client (connection errors etc), or if a 500-range
// response is received, then a retry is invoked. Otherwise, the response is
// returned and left to the caller to interpret.


import (
	"fmt"
	//"log"
	"math"
	"net/http"
	"time"
	//"bytes"
	"bufio"
	"bytes"
)

var (
	// Default retry configuration
	defaultRetryWaitMin = 1 * time.Second
	defaultRetryWaitMax = 30 * time.Second
	defaultRetryMax = 25
	defaultRequest = NewRequest()
	// We need to consume response bodies to maintain http connections, but
	// limit the size we consume to respReadLimit.
)
type Payload struct {
	UserID string
	Limit int
}
func Get(Payload) ([]byte, error)
func ParseResponse([]byte) ([]string, error)

// CheckRetry specifies a policy for handling retries.
// If CheckRetry returns false, the Client stops retrying
// and returns the response to the caller.
type CheckRetry func(resp *http.Response, err error) (bool, error)

type ReqBack struct {
	// Minimum time to wait
	MinWait    time.Duration
	// Maximum time to wait
	MaxWait    time.Duration
	// Maximum number of retries.Duration
	MaxRetry   int
	// CheckRetry specifies the policy for handling retries, and is called
	// after each request.
	CheckRetry CheckRetry
}
func NewRequest() *ReqBack {
	return &ReqBack{
		MinWait: defaultRetryWaitMin,
		MaxWait: defaultRetryWaitMax,
		MaxRetry:     defaultRetryMax,
		CheckRetry:   DefaultRetryPolicy,
	}
}

// DefaultRetryPolicy provides a default callback for ReqBack.CheckRetry, which
// will retry on connection errors and server errors.
func DefaultRetryPolicy(resp *http.Response, err error) (bool, error) {
	if err != nil {
		return true, err
	}
	// Check the response code. >=500 is invalid too
	if resp.StatusCode == 0 || resp.StatusCode >= 500 {
		return true, nil
	}

	return false, nil
}
// Do wraps calling an HTTP method with retries.
func (rb *ReqBack) GetDo(req *Payload) (*http.Response, error) {

	for i := 0; ; i++ {
		var code int // HTTP response code

		// Attempt the request
		resp, err := Get(*req)
		// Magic for reading http raw in http.ReadResponce
		readbuffer := bytes.NewBuffer(resp)
		reader := bufio.NewReader(readbuffer)
		httresponse, err := http.ReadResponse(reader,nil)
		// Check if we should continue with retries.
		checkOK, checkErr := rb.CheckRetry(httresponse, err)

		if err != nil {
			fmt.Printf("[ERR] %s %s request failed: %v", req, err)
		}
		// Check for continue.
		if !checkOK {
			if checkErr != nil {
				err = checkErr
			}
			return httresponse, err
		}

		remain := rb.MaxRetry - i
		if remain == 0 {
			break
		}
		wait := backoff(rb.MinWait, rb.MaxWait, i)
		desc := fmt.Sprintf("%s", req)
		if code > 0 {
			desc = fmt.Sprintf("%s (status: %d)", desc, code)
		}
		fmt.Printf("[DEBUG] %s: retrying in %s (%d left)", desc, wait, remain)
		time.Sleep(wait)
	}

	// Return an error if we fall out of the retry loop
	return nil, fmt.Errorf("%s  giving up after %d attempts",
		req, rb.MaxRetry + 1)
}


// backoff is used to calculate how long to sleep before retrying
// after observing failures. It takes the minimum/maximum wait time and
// iteration, and returns the duration to wait.
func backoff(min, max time.Duration, iter int) time.Duration {
	mult := math.Pow(2, float64(iter)) * float64(min)
	sleep := time.Duration(mult)
	if float64(sleep) != mult || sleep > max {
		sleep = max
	}
	return sleep
}
