package jfapi
import (
	"encoding/csv"
	"log"
	"os"
	"fmt"
	"io"
)
func CsvWriter(out *[]string, file string)(*os.File, error) {
	var header = []string{"USERID", "Subject", "Data"}
	f, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal("Error open file", err)
	}
	defer f.Close()

	reader := csv.NewReader(f)
	reader.Comma = ';'
	writer:= csv.NewWriter(f)
	writer.Comma = ';' // Use ";" instead of comma <---- here!
	record, err := reader.Read()
	// end-of-file is fitted into err
	if err == io.EOF && record == nil {
		if err := writer.Write(header); err != nil {
			log.Fatalln("error writing record to csv:", err)
			return nil, err
		}
	} else if err != nil {
		fmt.Println("Error:", err)
		return nil, err
	}
	if err := writer.Write(*out); err != nil {
		log.Fatalln("error writing record to csv:", err)
		return nil, err
	}
	// Write any buffered data to the underlying writer (standard output).
	writer.Flush()

	if err := writer.Error(); err != nil {
		log.Fatal(err)
		return nil,err
	}
	return f, nil
}
